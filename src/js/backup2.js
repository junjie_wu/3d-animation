var aaa = document.querySelector('a-sphere');
// console.log(aaa)
aaa.addEventListener('click', function (evt) {
  console.log("clicked" + aaa);
});

var product = document.querySelector('#product');
var el = document.querySelector('a-sphere');

// //Germany

// AFRAME.registerComponent("animation1", {
//   init: function () {
//     let anim = document.querySelector("#animation1")
//     let button1 = document.querySelector('button:nth-child(1)');
//     el.addEventListener("click", (e) => {
//      anim.emit("animation1")
// console.log("hello")
//     })

//   }
// })


document.querySelector("button:nth-child(1)").addEventListener("click", () => {
  el.emit("rotation-pause")
  document.querySelector("#animation1").emit("animation1")
  setTimeout(() => {
     document.querySelector('#product').object3D.visible = true;
    document.querySelector('#location-germany').object3D.visible = true;
    document.querySelector('#text-germany').object3D.visible = true;
    el.setObject3D('light', new THREE.PointLight(0xFFFFFF, 1, 10));
    el.object3D.scale.set(1.1, 1.1, 1.1);
  }, 1000)
});

document.querySelector("button:nth-child(2)").addEventListener("click", () => {
  el.emit("rotation-pause")
  document.querySelector("#animation2").emit("animation2")
  setTimeout(() => {
  document.querySelector('#product').object3D.visible = true;
  document.querySelector('#location-Britain').object3D.visible = true;
  document.querySelector('#text-Britain').object3D.visible = true;
  el.setObject3D('light', new THREE.PointLight(0xFFFFFF, 1, 1000));
  el.object3D.scale.set(1.1, 1.1, 1.1);
}, 1000)
});

document.querySelector("button:nth-child(3)").addEventListener("click", () => {
  el.emit("rotation-pause")
  document.querySelector("#animation3").emit("animation3")
  setTimeout(() => {
  document.querySelector('#product').object3D.visible = true;
  document.querySelector('#location-Spain').object3D.visible = true;
  document.querySelector('#text-Spain').object3D.visible = true;
  el.setObject3D('light', new THREE.PointLight(0xFFFFFF, 1, 1000));
  el.object3D.scale.set(1.1, 1.1, 1.1);
}, 1000)
});

document.querySelector("button:nth-child(4)").addEventListener("click", () => {
  el.emit("rotation-pause") 
  document.querySelector("#animation4").emit("animation4")
  setTimeout(() => {
  document.querySelector('#product').object3D.visible = true;
  document.querySelector('#location-Finland').object3D.visible = true;
  document.querySelector('#text-Finland').object3D.visible = true;
  el.setObject3D('light', new THREE.PointLight(0xFFFFFF, 1, 1000));
  el.object3D.scale.set(1.1, 1.1, 1.1);
}, 1000)
})


// // animation5

// AFRAME.registerComponent("animation5", {
//   init: function () {
//     let anim = document.querySelector("#animation5")

//     document.querySelector('#product').addEventListener("click", (e) => {
//       // el.emit("animation1")
//       anim.emit("animation5")
//     })

//   }
// })

document.getElementById("product").addEventListener('click', function (evt) {
  var el = document.querySelector('a-sphere');
  el.emit("rotation-resume");
  document.querySelector('#product').object3D.visible = false;
  // document.querySelector('.location-indicator').object3D.visible = false;
  var indicators = document.getElementsByClassName('location-indicator');
  for (var i = 0; i < 4; i++) {
    indicators[i].object3D.visible = false;
  }
  var texts = document.getElementsByClassName('text');
  // console.log(texts);
  for (var i = 0; i < 4; i++) {
    texts[i].object3D.visible = false;
  }
  el.setObject3D('light', new THREE.PointLight(0));
  el.object3D.scale.set(1, 1, 1);
})