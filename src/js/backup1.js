// AFRAME.registerComponent('closeboard', {
//   schema: {
//     color: {default: 'red'}
//   },

//   init: function () {
//     var data = this.data;
//     var el = this.el;  
//     var defaultColor = el.getAttribute('color');

//     el.addEventListener('click', function () {
//       console.log("..");
//     });

//     el.addEventListener('mouseleave', function () {
//       el.setAttribute('color', defaultColor);
//     });
//   }

// });

AFRAME.registerComponent('cursor-listener', {
    init: function () {
      this.el.addEventListener('click', function (evt) {
        this.setAttribute('material', 'color', COLORS[randomIndex]);
        console.log('I was clicked at: ', evt.detail.intersection.point);
      });
    }
  });
  
  var aaa = document.querySelector('a-sphere');
  // console.log(aaa)
  aaa.addEventListener('click', function (evt) {
    console.log("clicked" + aaa);
  });
  
  var sphere = document.querySelector('a-sphere');
  
  
  // AFRAME.registerComponent('rotating-on-mouseenter', {
  //     schema: {
  //         to: {
  //             default: '2.5 2.5 2.5',
  //             type: 'vec3'
  //         }
  //     },
  
  //     init: function () {
  //         var data = this.data;
  //         var el = this.el;
  //         this.el.addEventListener('click', function () {
  //             el.object3D.scale.copy(data.to);
  //         });
  //     }
  // });
  
  // AFRAME.registerComponent('change-color-on-hover', {
  //     schema: {
  //         color: {
  //             default: 'red'
  //         }
  //     },
  
  //     init: function () {
  //         var data = this.data;
  //         var el = this.el; // <a-box>
  //         var defaultColor = el.getAttribute('material').color;
  
  //         el.addEventListener('mouseenter', function () {
  //             el.setAttribute('color', data.color);
  //         });
  
  //         el.addEventListener('mouseleave', function () {
  //             el.setAttribute('color', defaultColor);
  //         });
  //     }
  // });
  
  // AFRAME.registerComponent('rotate-to-germany', {
  //   init: function () {
  //     // This will be called after the entity has properly attached and loaded.
  //     console.log('I am ready!');
  //   }
  // });
  
  
  
  document.querySelector("button:nth-child(1)").addEventListener("click", () => {
    var el = document.querySelector('a-sphere');
    el.pause();
    console.log(el)
    // el.object3D.animation.set(
    //   THREE.Math.degToRad(0),
    //   THREE.Math.degToRad(190),
    //   THREE.Math.degToRad(0)
    // );
  
    setTimeout(() => {
      // el.emit('rotateToGermany');
      // el.object3D.rotation.x = 0;
      // el.object3D.rotation.y = 150;
      // el.object3D.rotation.x = 0;
      el.setAttribute('rotation', {
        x: 0,
        y: 190,
        z: 0
      });
      // el.setAttribute('animation', 'property: position; to: 0 0 0')
      // el.setAttribute('animation', "property: rotation; to:0 190 0; easing: linear; dur: 10000");
      document.querySelector('#product').object3D.visible = true;
      document.querySelector('#location-germany').object3D.visible = true;
      document.querySelector('#text-germany').object3D.visible = true;
      el.setObject3D('light', new THREE.PointLight(0xFFFFFF, 1, 10));
      el.object3D.scale.set(1.1, 1.1, 1.1);
    }, 1000)
  
    // el.rotateOnAxis(new THREE.Vector3(0,0,1), 90*Math.PI/180); 
  
  
    // console.log(el.object3D.rotation);
    // el.object3D.rotation.set(
    //   loop = false
    //   );
    // el.object3D.rotation._x = 20;
    // el.object3D.rotation.loop = false;
  
    el.object3D.scale.set(1.1, 1.1, 1.1);
  
    // //   el.object3D.rotation.x += Math.PI;
  });
  
  document.querySelector("button:nth-child(2)").addEventListener("click", () => {
    var el = document.querySelector('a-sphere');
    el.pause();
    el.setAttribute('rotation', {
      x: -30,
      y: 250,
      z: 0
    });
    document.querySelector('#product').object3D.visible = true;
    document.querySelector('#location-Britain').object3D.visible = true;
    document.querySelector('#text-Britain').object3D.visible = true;
    el.setObject3D('light', new THREE.PointLight(0xFFFFFF, 1, 1000));
    el.object3D.scale.set(1.1, 1.1, 1.1);
  });
  
  document.querySelector("button:nth-child(3)").addEventListener("click", () => {
    var el = document.querySelector('a-sphere');
    el.pause();
    el.setAttribute('rotation', {
      x: 10,
      y: 30,
      z: 0
    });
    document.querySelector('#product').object3D.visible = true;
    document.querySelector('#location-Spain').object3D.visible = true;
    document.querySelector('#text-Spain').object3D.visible = true;
    el.setObject3D('light', new THREE.PointLight(0xFFFFFF, 1, 1000));
    el.object3D.scale.set(1.1, 1.1, 1.1);
  });
  
  document.querySelector("button:nth-child(4)").addEventListener("click", () => {
    var el = document.querySelector('a-sphere');
    // el.pause();
    el.setAttribute("animation", "enabled", false)
    el.setAttribute('rotation', {
      x: -20,
      y: 50,
      z: 0
    });
    document.querySelector('#product').object3D.visible = true;
    document.querySelector('#location-Finland').object3D.visible = true;
    document.querySelector('#text-Finland').object3D.visible = true;
    // el.setObject3D('light', new THREE.PointLight(0xFFFFFF, 1, 1000));
    el.object3D.scale.set(1.1, 1.1, 1.1);
  })
  
  document.getElementById("product").addEventListener('click', function (evt) {
    var el = document.querySelector('a-sphere');
  
       el.emit('animation1');
      // var dfs = document.querySelector("#aniContinue")
      // dfs.emit('rotationContinue');
      // console.log(dfs)
  
    // AFRAME.registerComponent('updateAnimation', {
    //   init: function () {
        // el.setAttribute('animation', {
        //   property: "scale", to: "2 2 2"
        //  }); 
    //   }
    // });
    // document.querySelector('#animation1').object3D.enabled = true;
  
    // el.setAttribute("animation__1", {enabled: true});
  
    document.querySelector('#product').object3D.visible = false;
    // document.querySelector('.location-indicator').object3D.visible = false;
    var indicators = document.getElementsByClassName('location-indicator');
    for (var i = 0; i < 4; i++) {
      indicators[i].object3D.visible = false;
    }
    var texts = document.getElementsByClassName('text');
    // console.log(texts);
    for (var i = 0; i < 4; i++) {
      texts[i].object3D.visible = false;
    }
    el.setObject3D('light', new THREE.PointLight(0));
    el.object3D.scale.set(1, 1, 1);
  })
  
  // document.querySelector("button:nth-child(5)").addEventListener("click", () => {
  //   var el = document.querySelector('a-sphere');
  //   // el.setAttribute('scale', {
  //   //  property: "rotation", to: "0 360 0", dur: 10000, easing: "linear", loop: true
  //   // }); 
  //   document.querySelector('#product').object3D.visible = false;
  //   // document.querySelector('.location-indicator').object3D.visible = false;
  //   var indicators = document.getElementsByClassName('location-indicator');
  //   for (var i = 0; i < 4; i++) {
  //     indicators[i].object3D.visible = false;
  //   }
  //   var texts = document.getElementsByClassName('text');
  //   // console.log(texts);
  //   for (var i = 0; i < 4; i++) {
  //     texts[i].object3D.visible = false;
  //   }
  //   el.setObject3D('light', new THREE.PointLight(0));
  //   el.object3D.scale.set(1, 1, 1);
  // })